# PasteHexoURL

`PasteHexoURL` is a simple extension that generating Markdown or reStructuredText style link when pasting URL.

> Check out [markdown-linker-expander](https://github.com/Skn0tt/markdown-link-expander) too, it solves the same problem in a different way! :D
## Features

A Markdown or reStructuredText inline-style link will be generated when pasting a URL into the file with corresponding active language.

For example, you copied the below URL:

    https://code.visualstudio.com

When pasting with `Paste URL`, you will get:

    {% link Visual Studio Code - Code Editing. Redefined https://code.visualstudio.com %}

One gif is worth a thousand words.

![feature](https://gitee.com/Ravenfu/paste-hexo-url/blob/master/images/screenshot.gif)

## Usage

For Ubuntu Linux make sure that xclip package is installed, see http://github.com/xavi-/node-copy-paste for details.

- Hit `"Control + Alt + P"` (Recommended)
- Hit `"Command + Shift + P"` and then type `Paste URL` and hit enter.

Selection will be used as the title if possible.

You can change the default shortcut to whatever you like by editing the `Code > Preferences > Keyboard Shortcuts`    (`File > Preferences > Keyboard Shortcuts` on Windows):

```json
[
    {"key": "ctrl+alt+p", "command": "pasteHexoURL.PasteHexoURL"}
]
```

## Known Issues

- Fetching title from some URLs may take longer than expected.

## Source

[Gitee](https://gitee.com/Ravenfu/paste-hexo-url.git)

## License

[MIT](https://gitee.com/Ravenfu/paste-hexo-url/blob/master/LICENSE)